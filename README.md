# Análise de Sentimento: Música Religiosa
Repositório de uma análise de sentimento de músicas católicas e evangélicas (protestantes).

Objetivos almejados:
    - Qualificar quais os sentimentos, idéias, mentalidades existente nas letras de cada estilo
    - Analisar as qualidades musicais (melodia, harmonia, ritmo, tom) de cada estilo
